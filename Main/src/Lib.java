import java.util.Iterator;
import java.util.List;

public class Lib {
    public static City getCityByName(List<City> c, String s) {
        Iterator<City> iter = c.iterator();
        while (iter.hasNext()) {
            City cx = iter.next();
            if (cx.getName().equals(s))
                return cx;
        }
        return null;
    }

    public static AreaObject getAreaObjectByName(List<City> c, String s, String s2) {
        City cx = Lib.getCityByName(c,s);
        if (cx == null)
            return null;

        if (cx.getObjectList() == null)
            return null;

        Iterator<AreaObject> iter = cx.getObjectList().iterator();
        while (iter.hasNext()) {
            AreaObject ao = iter.next();
            if (ao.getName().equals(s2))
                return ao;
        }

        return null;
    }


    public static class JT {
        public static long sumAreaSize(List<JobTask> tl) {
            long size = 0;
            Iterator<JobTask> iter = tl.iterator();
            while (iter.hasNext()) {
                JobTask t = iter.next();
                size += t.getAreaSize();
            }
            return size;
        }

        public static long sumLength(List<JobTask> tl) {
            long len = 0;
            Iterator<JobTask> iter = tl.iterator();
            while (iter.hasNext()) {
                JobTask t = iter.next();
                len += t.getLength();
            }
            return len;
        }

    }



}
