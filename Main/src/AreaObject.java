public class AreaObject {
    enum ObjectType {
        STREET, DISTRICT, RIVER
    };

    private String name;
    private ObjectType type;

    private long sizeX;
    private long sizeY;

    public String getName() {
        return name;
    }

    public AreaObject setName(String name) {
        this.name = name;
        return this;
    }

    public ObjectType getType() {
        return type;
    }

    public AreaObject setType(ObjectType type) {
        this.type = type;
        return this;
    }

    public long getSizeX() {
        return sizeX;
    }

    public AreaObject setSizeX(long sizeX) {
        this.sizeX = sizeX;
        return this;
    }

    public long getSizeY() {
        return sizeY;
    }

    public AreaObject setSizeY(long sizeY) {
        this.sizeY = sizeY;
        return this;
    }
}
