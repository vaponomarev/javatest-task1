public class JobTask {
    enum State {
        NEW,
        ASSIGNED,
        IN_PROGRESS,
        PENDING,
        COMPLETE,
        REJECTED
    };

    // Характетистики
    private String title;
    private long length;
    private long areaSize;

    // Статус
    private State state;

    // Привязка к городу
    private City cityRef;

    // Привязка к объекту
    private AreaObject areaObjectRef;

    public JobTask setTitle(String title) {
        this.title = title;
        return this;
    }

    public JobTask setLength(long length) {
        this.length = length;
        return this;
    }

    public JobTask setAreaSize(long areaSize) {
        this.areaSize = areaSize;
        return this;
    }

    public JobTask setState(State state) {
        this.state = state;
        return this;
    }

    public JobTask setCityRef(City cityRef) {
        this.cityRef = cityRef;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public long getLength() {
        return length;
    }

    public long getAreaSize() {
        return areaSize;
    }

    public State getState() {
        return state;
    }

    public City getCityRef() {
        return cityRef;
    }

    public AreaObject getAreaObjectRef() {
        return areaObjectRef;
    }

    public JobTask setAreaObjectRef(AreaObject areaObjectRef) {
        this.areaObjectRef = areaObjectRef;
        return this;
    }
}
