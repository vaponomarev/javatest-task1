import java.util.List;

public class City {

    private String name = "";
    private long size = 0;
    private List<AreaObject> objectList;

    public String getName() {
        return name;
    }

    public long getSize() {
        return size;
    }

    public City setName(String name) {
        this.name = name;
        return this;
    }

    public City setSize(long size) {
        this.size = size;
        return this;
    }

    public List<AreaObject> getObjectList() {
        return objectList;
    }

    public City setObjectList(List<AreaObject> objectList) {
        this.objectList = objectList;
        return this;
    }
}
