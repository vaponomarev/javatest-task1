import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // Cities
        List<City> cList = new ArrayList<City>();

        // Area objects - for Хабаровск
        List<AreaObject> oList = new ArrayList<AreaObject>();
        oList.add((new AreaObject()).setName("Ленина").setType(AreaObject.ObjectType.STREET).setSizeX(100).setSizeY(10));
        cList.add((new City()).setName("Хабаровск").setSize(512).setObjectList(oList));

        // Area objects - for Ростов
        oList = new ArrayList<AreaObject>();
        oList.add((new AreaObject()).setName("Ленина").setType(AreaObject.ObjectType.STREET).setSizeX(100).setSizeY(10));
        cList.add((new City()).setName("Ростов").setSize(847).setObjectList(oList));

        // Area objects - for Москва
        oList = new ArrayList<AreaObject>();
        oList.add((new AreaObject()).setName("Ленина").setType(AreaObject.ObjectType.STREET).setSizeX(100).setSizeY(10));
        cList.add((new City()).setName("Москва").setSize(10452).setObjectList(oList));




        List<JobTask> tList = new ArrayList<JobTask>();
        tList.add(
                (new JobTask())
                        .setTitle("Ремонт дороги")
                        .setAreaSize(50)
                        .setLength(100)
                        .setState(JobTask.State.NEW)
                        .setCityRef(Lib.getCityByName(cList, "Москва"))
                        .setAreaObjectRef(Lib.getAreaObjectByName(cList, "Москва", "Ленина"))
        );

        tList.add(
                (new JobTask())
                        .setTitle("Ремонт дороги")
                        .setAreaSize(15)
                        .setLength(80)
                        .setState(JobTask.State.NEW)
                        .setCityRef(Lib.getCityByName(cList, "Новосибирск"))
                        .setAreaObjectRef(Lib.getAreaObjectByName(cList, "Новосибирск", "Ленина"))
        );


        System.out.println(
                "Overall size: " + Lib.JT.sumAreaSize(tList) +
                ", total length: " + Lib.JT.sumLength(tList) +
                ", Number of damaged objects: " + tList.size());

        City x;
        if ((x = Lib.getCityByName(cList, "Москва")) != null) {
            System.out.println("Found: " + x.getName());
        }


        Iterator<City> iter = cList.iterator();
        while (iter.hasNext()) {
            City cx = iter.next();
            System.out.println(cx.getName());

            // if (cx.getName().equals("Ростов"))
            //    iter.remove();
        }
        /*
        for (City c : cList) {
            System.out.println("> " + c.getName());
        }
        */


    }
}
